package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

public class SubOperationTest extends HelperClass {
    By digit7 = By.id("digit_7");
    By digit8 = By.id("digit_8");
    By minus = By.id("op_sub");
    By equal = By.id("eq");
    By result = By.id("result");

    public SubOperationTest(WebDriver driver) {
        super(driver);
    }

    public SubOperationTest subOperationTest(){
        RunCases.test = RunCases.extent.createTest("Sub operation test");
        driver.findElement(digit8).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 8", ExtentColor.CYAN));
        driver.findElement(minus).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on -", ExtentColor.CYAN));
        driver.findElement(digit7).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 7", ExtentColor.CYAN));
        driver.findElement(equal).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on =", ExtentColor.CYAN));

        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Expected Result: 1", ExtentColor.CYAN));
        String actualResult = driver.findElement(result).getText();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Actual Result: "+actualResult, ExtentColor.CYAN));
        Assert.assertEquals(actualResult, "1");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Sub operation PASS", ExtentColor.GREEN));

        return new SubOperationTest(driver);
    }
}
