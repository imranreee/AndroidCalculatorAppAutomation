package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

public class MulOperationTest extends HelperClass {
    By digit7 = By.id("digit_7");
    By digit8 = By.id("digit_8");
    By multiple = By.id("op_mul");
    By equal = By.id("eq");
    By result = By.id("result");

    public MulOperationTest(WebDriver driver) {
        super(driver);
    }

    public MulOperationTest mulOperationTest(){
        RunCases.test = RunCases.extent.createTest("Mul operation test");
        waitForVisibilityOf(digit7);
        driver.findElement(digit7).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 7", ExtentColor.CYAN));
        driver.findElement(multiple).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on *", ExtentColor.CYAN));
        driver.findElement(digit8).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 8", ExtentColor.CYAN));
        driver.findElement(equal).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on =", ExtentColor.CYAN));

        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Expected Result: 56", ExtentColor.CYAN));
        String actualResult = driver.findElement(result).getText();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Actual Result: "+actualResult, ExtentColor.CYAN));
        Assert.assertEquals(actualResult, "56");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Mul operation Pass", ExtentColor.GREEN));

        return new MulOperationTest(driver);
    }
}
