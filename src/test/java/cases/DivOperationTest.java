package cases;

import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import settings.HelperClass;
import settings.RunCases;

public class DivOperationTest extends HelperClass {
    By digit2 = By.id("digit_2");
    By digit8 = By.id("digit_8");
    By divide = By.id("op_div");
    By equal = By.id("eq");
    By result = By.id("result");

    public DivOperationTest(WebDriver driver) {
        super(driver);
    }

    public DivOperationTest divOperationTest(){
        RunCases.test = RunCases.extent.createTest("Div operation test");
        waitForVisibilityOf(digit8);
        driver.findElement(digit8).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 8", ExtentColor.CYAN));
        driver.findElement(divide).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on /", ExtentColor.CYAN));
        driver.findElement(digit2).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on 2", ExtentColor.CYAN));
        driver.findElement(equal).click();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Clicked on =", ExtentColor.CYAN));

        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Expected Result: 4", ExtentColor.CYAN));
        String actualResult = driver.findElement(result).getText();
        RunCases.test.log(Status.INFO, MarkupHelper.createLabel("Actual Result: "+actualResult, ExtentColor.CYAN));
        Assert.assertEquals(actualResult, "4");
        RunCases.test.log(Status.PASS, MarkupHelper.createLabel("Div operation Pass", ExtentColor.GREEN));

        return new DivOperationTest(driver);
    }
}
