package settings;

import io.appium.java_client.android.AndroidDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by imran on 2/18/2017.
 */
public class AndroidSetup {
    protected static AndroidDriver driver;

    public static void prepareAndroidForAppium() throws MalformedURLException {
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setCapability("appiumVersion", "1.4.16"); //Optional
        capabilities.setCapability("platformName", "Android"); //Optional
        capabilities.setCapability("platformVersion", "7.0");
        capabilities.setCapability("deviceName", "Nexus");

        capabilities.setCapability("app", "D:\\DemoProject\\AndroidCalculatorAppAutomation\\testApk\\calculator.apk");
        capabilities.setCapability("appPackage", "com.google.android.calculator");
        capabilities.setCapability("appActivity", "com.android.calculator2.CalculatorGoogle");
        driver = new AndroidDriver(new URL("http://localhost:4723/wd/hub"), capabilities);
    }
}
