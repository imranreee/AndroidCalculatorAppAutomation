package settings;


import cases.AddOperationTest;
import cases.DivOperationTest;
import cases.MulOperationTest;
import cases.SubOperationTest;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.ChartLocation;
import com.aventstack.extentreports.reporter.configuration.Theme;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by imran on 12/09/2017.
 */
public class RunCases extends AndroidSetup {
    public static ExtentHtmlReporter htmlReporter;
    public static ExtentReports extent;
    public static ExtentTest test;
    String versionNumber = new String("0.0.1");  //Put the app versing here

    DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy__hh-mm-ssaa");
    Date date = new Date();
    String currentDate = dateFormat.format(date);
    DateFormat dateFormatNew = new SimpleDateFormat("dd-MMM-yyyy");
    Date dateNew = new Date();
    String currentDateNew = dateFormatNew.format(dateNew);

    @BeforeClass
    public void setUp() throws Exception {
        prepareAndroidForAppium();
    }

    @BeforeTest
    public void config() throws Exception {
        File file = new File("D:\\DemoProject\\AndroidCalculatorAppAutomation\\testResults\\" + currentDateNew);
        if (!file.exists()) {
            file.mkdir();
        } else {

        }
        htmlReporter = new ExtentHtmlReporter(file + "\\CalculatorAutomationTestResult_" + versionNumber + "_" + currentDate + ".html");
        extent = new ExtentReports();
        extent.attachReporter(htmlReporter);

        extent.setSystemInfo("Machine", "Windows");
        extent.setSystemInfo("Platform Name", "Android");
        extent.setSystemInfo("Application Name", "Calculator");
        extent.setSystemInfo("Application Version", versionNumber);
        extent.setSystemInfo("Script written by", "Al Imran");

        htmlReporter.config().setChartVisibilityOnOpen(true);
        htmlReporter.config().setDocumentTitle("Automation Test Results");
        htmlReporter.config().setReportName("Automation Test Results of Calculator_"+versionNumber+" Android App");
        htmlReporter.config().setTestViewChartLocation(ChartLocation.TOP);
        htmlReporter.config().setTheme(Theme.DARK);
    }

    @Test(priority = 0)
    public void addOperationTest(){
        new AddOperationTest(driver).addOperationTest();
    }

    @Test(priority = 1)
    public void subOperationTest(){
        new SubOperationTest(driver).subOperationTest();
    }

    @Test(priority = 2)
    public void mulOperationTest(){
        new MulOperationTest(driver).mulOperationTest();
    }

    @Test(priority = 3)
    public void divOperationTest(){
        new DivOperationTest(driver).divOperationTest();
    }

    @AfterMethod
    public void getResult(ITestResult result) throws IOException {
        if (result.getStatus() == ITestResult.FAILURE) {
            String screenShotPath = CaptureScreenShot.capture(driver, result.getName() + "_" + currentDate);
            test.log(Status.FAIL, MarkupHelper.createLabel(result.getName() + "Test case FAILED due to below issues:", ExtentColor.RED));
            test.fail(result.getThrowable());
            test.addScreenCaptureFromPath(screenShotPath);

        } else if (result.getStatus() == ITestResult.SUCCESS) {
            test.log(Status.PASS, MarkupHelper.createLabel(result.getName() + "Test Case PASSED", ExtentColor.GREEN));
        } else {
            test.log(Status.SKIP, MarkupHelper.createLabel(result.getName() + "Test Case SKIPPED", ExtentColor.ORANGE));
            test.skip(result.getThrowable());
        }
    }

    @AfterTest
    public void tearDown() {
        extent.flush();
    }
}
